<?php

namespace Idrd\Usuarios\Repo;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Config as Config;

class Eps extends Eloquent {

	protected $table = 'eps';
	protected $primaryKey = 'Id_Eps';
	protected $fillable = ['Nombre_Eps', 'estado'];
	protected $connection = '';
	public $timestamps = false;

	public function __construct()
	{
		$this->connection = config('usuarios.conexion');
	}
}
