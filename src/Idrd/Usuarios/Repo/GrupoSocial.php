<?php

namespace Idrd\Usuarios\Repo;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Config as Config;

class GrupoSocial extends Eloquent {
	
	protected $table = 'social_poblacional';
	protected $primaryKey = 'id';
	protected $fillable = ['nombre'];
	protected $connection = '';
	public $timestamps = false;

	public function __construct()
	{
		$this->connection = config('usuarios.conexion');
	}

	public function personas()
	{
		return $this->belongsToMany(config('usuarios.modelo_persona'), 'personas_grupos_sociales_poblacionales', 'id_grupo', 'id_persona');
	}
}