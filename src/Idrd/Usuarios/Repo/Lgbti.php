<?php

namespace Idrd\Usuarios\Repo;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Config as Config;

class Lgbti extends Eloquent {
	
	protected $table = 'lgbti';
	protected $primaryKey = 'id';
	protected $fillable = ['lgbti'];
	protected $connection = '';
	public $timestamps = false;

	public function __construct()
	{
		$this->connection = config('usuarios.conexion');
	}

}