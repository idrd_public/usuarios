<?php

namespace Idrd\Usuarios\Repo;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Config as Config;

class IdentidadDeGenero extends Eloquent {
	
	protected $table = 'identidad_de_genero';
	protected $primaryKey = 'id';
	protected $fillable = ['identidad'];
	protected $connection = '';
	public $timestamps = false;

	public function __construct()
	{
		$this->connection = config('usuarios.conexion');
	}

	public function personas()
	{
		return $this->hasMany(config('usuarios.modelo_persona'), 'Id_Identidad_Genero');
	}
}