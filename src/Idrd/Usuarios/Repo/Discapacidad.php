<?php

namespace Idrd\Usuarios\Repo;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Config as Config;

class Discapacidad extends Eloquent {
	
	protected $table = 'discapacidad';
	protected $primaryKey = 'id';
	protected $fillable = ['discapacidad'];
	protected $connection = '';
	public $timestamps = false;

	public function __construct()
	{
		$this->connection = config('usuarios.conexion');
	}

}