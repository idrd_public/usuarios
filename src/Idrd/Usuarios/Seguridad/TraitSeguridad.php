<?php

namespace Idrd\Usuarios\Seguridad;

trait TraitSeguridad {

  public static function boot()
 {
    parent::boot();

    static::creating(function($model)
    {
     \DB::connection('db_principal')->table('seguridad')->insert([
        'Id_Modulo' => config('usuarios.modulo'),
        'Fecha' => date('Y-m-d H:i:s'),
        'Id_Persona' => empty($_SESSION['Usuario'][0]) ? config('usuarios.fake_id_persona') : $_SESSION['Usuario'][0],
        'Tabla' => $model->getTableName(),
        'Operacion' =>'create' ,
        'antes'=>'',
        'ahora' => $model->toJson(),
        'Ip' => array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : '120.0.0.1'
      ]);
    });

    static::updating(function($model)
    {

     \DB::connection('db_principal')->table('seguridad')->insert([
        'Id_Modulo' => config('usuarios.modulo'),
        'Fecha' => date('Y-m-d H:i:s'),
        'Id_Persona' => empty($_SESSION['Usuario'][0]) ? config('usuarios.fake_id_persona') : $_SESSION['Usuario'][0],
        'Tabla' => $model->getTableName(),
        'Operacion' => 'update' ,
        'antes'=>'',
        'ahora' => $model->toJson(),
        'Ip' => array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : '120.0.0.1'
      ]);
    });

     static::deleting(function($model)
    {

     \DB::connection('db_principal')->table('seguridad')->insert([
        'Id_Modulo' =>config('usuarios.modulo'),
        'Fecha' => date('Y-m-d H:i:s'),
        'Id_Persona' => empty($_SESSION['Usuario'][0]) ? config('usuarios.fake_id_persona') : $_SESSION['Usuario'][0],
        'Tabla' => $model->getTableName(),
        'Operacion' => 'delete' ,
        'antes'=>'',
        'ahora' => $model->toJson(),
        'Ip' => array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : '120.0.0.1'
      ]);
    });
  }

  private function getTableName()
  {
    return $this->table;
  }

}


?>
